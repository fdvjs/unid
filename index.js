module.exports = exports = createGenerator;

exports.calcDelta = calcDelta;
exports.int53 = int53;
exports.jan1 = jan1;
exports.byte = byte;
exports.pentad = pentad;

exports.DEFAULT_EPOCH = jan1(2019).getTime();
exports.DEFAULT_WORKER = 0;

function createGenerator ({
  worker = exports.DEFAULT_WORKER,
	epoch = exports.DEFAULT_EPOCH
} = {}) {
  if (!Number.isSafeInteger(worker) || worker < 0 || worker > 8) {
    throw new Error('Worker must be an integer in range [0, 8]');
  }

  if (!Number.isSafeInteger(epoch) || epoch < 0 || epoch > Date.now()) {
    throw new Error('Epoch must be an integer in range [0, Date.now()]');
  }

  const buff = Buffer.alloc(5);
  let past, seq;

  return function next () {
	  let now = Date.now();

	  if (now !== past) {
	    past = now;
	    seq = 0;
	  } else if (seq < 31) {
	    seq++;
	  } else {
      sleep1ms();
      return next();
	  }

	  const delta = calcDelta(now, epoch);
	  buff.writeUIntBE(delta, 0, 5);

    const rnd = (Math.random() * 32) | 0;
    const id = int53(seq,
                     buff.readUInt8(4),
                     buff.readUInt8(1),
                     rnd | (worker << 5),
                     buff.readUInt8(3),
                     buff.readUInt8(0),
                     buff.readUInt8(2));

    return id;
  }
}

function calcDelta (now, epoch) {
  const delta = now - epoch;

  if (delta > 0xffffffffff) {
    throw new Error('Timestamp overflow');
  }

  return delta;
}

const sleep1ms = (function createSleep1ms () {
  if (typeof Atomics === 'undefined' ||
      typeof Int32Array === 'undefined' ||
      typeof SharedArrayBuffer === 'undefined') {
    return function sleep1ms () {
      const now = Date.now();
      while (Date.now() = now) {}
    }
  }

  return Atomics.wait.bind(
    Atomics, new Int32Array(new SharedArrayBuffer(4)), 0, 0, 1
  );
})();

function int53 (pentad0, byte1, byte2, byte3, byte4, byte5, byte6) {
  const quadlet0 = byte(byte6)
        | (byte(byte5) << 8)
        | (byte(byte4) << 16);

  const quadlet1 = byte(byte3)
        | (byte(byte2) << 8)
        | (byte(byte1) << 16)
        | (pentad(pentad0) << 24);

  return quadlet1 * 0x1000000 + quadlet0;
}

function jan1 (year) {
  const d = new Date();
  d.setUTCFullYear(year);
  d.setUTCMonth(0);
  d.setUTCDate(1);
  d.setUTCHours(0);
  d.setUTCMinutes(0);
  d.setUTCSeconds(0);
  d.setUTCMilliseconds(0);
  return d;
}

function byte (n) {
  if (0x00 <= n && n <= 0xff) {
    return n;
  }

  throw new Error('ID overflow. Could not generate safe integer ID' + n);
}

function pentad (n) {
  if (0b00000 <= n && n <= 0b11111) {
    return n;
  }

  throw new Error('ID overflow. Could not generate safe integer ID' + n);
}
