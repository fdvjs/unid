# Unid
## [Yet another] Unique, Safe, Integer ID Generator for Node.js


## Usage
```javascript
const unid = require('@fdv/unid');
const nextId = unid({ /* epoch: SOME_DATE, worker: SOME_ID */ });

nextId().then(id => console.log(id));

// or:
(async function () {
  const id = await nextId();
  // ...
})();
```

## License
   WTFPLv2
